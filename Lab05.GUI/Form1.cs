﻿using Lab05.BUS;
using Lab05.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab05.GUI
{
    
    public partial class FrmQuanLySinhVien : Form
    {
        private readonly StudentService studentService = new StudentService();
        private readonly FacultyService facultyService = new FacultyService();
        public FrmQuanLySinhVien()
        {
            InitializeComponent();
        }

        private void FrmQuanLySinhVien_Load(object sender, EventArgs e)
        {
            
            try
            {
                cmbFaculty.DropDownStyle = ComboBoxStyle.DropDownList;
                setGridViewStyle(dgvData);
                var listFacultys = facultyService.GetAll();
                var listStudents = studentService.GetAll();
                FillFacultyCombobox(listFacultys);
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BindGrid(List<Student> listStudent)
        {
            dgvData.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dgvData.Rows.Add();
                dgvData.Rows[index].Cells[0].Value = item.StudentID;
                dgvData.Rows[index].Cells[1].Value = item.FullName;
                if (item.Faculty != null)
                {
                    dgvData.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                    dgvData.Rows[index].Cells[3].Value = item.AverageScore + "";
                }
                if (item.Major != null)
                {
                    dgvData.Rows[index].Cells[4].Value = item.Major.Name + "";
                    ShowAvatar(item.Avatar);

                }
            }
        }

        private void ShowAvatar(string ImageName)
        {
            if (string.IsNullOrEmpty(ImageName))
            {
                picAvatar.Image = null;
            }
            else
            {
                //string parentDirectory = Directory
            }
        }

        private void FillFacultyCombobox(List<Faculty> listFacultys)
        {
            listFacultys.Insert(0, new Faculty());
            this.cmbFaculty.DataSource = listFacultys;
            this.cmbFaculty.DisplayMember = "FacultyName";
            this.cmbFaculty.ValueMember = "FacultyID";
        }

        private void setGridViewStyle(DataGridView dgview)
        {
            dgview.BorderStyle = BorderStyle.None;
            dgview.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgview.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgview.BackgroundColor = Color.White;
            dgview.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void chkChuaDKChuyenNganh_CheckedChanged(object sender, EventArgs e)
        {
            var listStudents = new List<Student>();
            if (this.chkChuaDKChuyenNganh.Checked)
                listStudents = studentService.GetAllHasNoMajor();
            else
                listStudents = studentService.GetAll();
            BindGrid(listStudents);
        }

        private void đăngKýChuyênNgànhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRegister frm = new frmRegister();
            frm.ShowDialog();
            this.Hide();
            this.Show();
        }

       
        private void AddStudent()
        {
            StudentModel context = new StudentModel();
            Student student = new Student()
            {
                StudentID = txtStudentID.Text,
                FullName = txtFullName.Text,
                AverageScore = float.Parse(txtAverageScore.Text),
                FacultyID = (int)cmbFaculty.SelectedValue,
            };
            context.Students.Add(student);
            context.SaveChanges();
            MessageBox.Show("Thêm mới dữ liệu thành công!");
        }
        private void Reset()
        {
            txtStudentID.Text = txtFullName.Text = txtAverageScore.Text = string.Empty;
            cmbFaculty.SelectedIndex = 0;
            
        }
        
        private void ReLoadDataGridView()
        {
            StudentModel context = new StudentModel();
            List<Student> students = context.Students.ToList();
            BindGrid(students);
        }

        
        private bool CheckError()
        {
            errorProvider1.Clear();
            if (string.IsNullOrEmpty(txtStudentID.Text))
            {
                
                errorProvider1.SetError(txtStudentID, "Vui lòng nhập đầy đủ thông tin!");
                return false;
            }
            if (txtStudentID.TextLength < 10)
            {
                errorProvider1.SetError(txtStudentID, "Mã số sinh viên phải có 10 kí tự!");
                return false;
            }
            if (string.IsNullOrEmpty(txtFullName.Text))
            {
                errorProvider1.SetError(txtFullName, "Vui lòng nhập đầy đủ thông tin!");
                return false;
            }
            if (txtFullName.TextLength < 3)
            {
                errorProvider1.SetError(txtFullName, "Họ tên phải nhiều hơn 3 ký tự!");
                return false;
            }
            if (txtFullName.TextLength > 100)
            {
                errorProvider1.SetError(txtFullName, "Họ tên phải ít hơn 100 ký tự!");
                return false;
            }
            if (string.IsNullOrEmpty(txtAverageScore.Text))
            {
                errorProvider1.SetError(txtAverageScore, "Vui lòng nhập đầy đủ thông tin!");
                return false;
            }
            float diemTB;
            float.TryParse(txtAverageScore.Text, out diemTB);
            if (diemTB < 0 || diemTB > 10)
            {
                errorProvider1.SetError(txtAverageScore, "Điểm trung bình không hợp lệ!!!");
                return false;
            }
            return true;
        }
        private void btnsua_Click(object sender, EventArgs e)
        {
            bool check = true;
            check = CheckError();
            if (check)
            {
                UpdateStudent();
                Reset();
                ReLoadDataGridView();
            }
        }

        private void UpdateStudent()
        {
            StudentModel context = new StudentModel();
            Student studentupdate = context.Students.FirstOrDefault(p => p.StudentID == txtStudentID.Text);
            if (studentupdate != null)
            {
                studentupdate.FullName = txtStudentID.Text;
                studentupdate.AverageScore = float.Parse(txtStudentID.Text);
                studentupdate.FacultyID = (int)cmbFaculty.SelectedValue;
                context.SaveChanges();
                MessageBox.Show("Cập nhật dữ liệu thành công!");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("MSSV chứa có trong dữ liệu!\nBạn có muốn thêm mới dữ liệu vào database?", "",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                    AddStudent();
            }
        }
        
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteStudent();
            Reset();
            ReLoadDataGridView();
        }
        private void DeleteStudent()
        {
            StudentModel context = new StudentModel();
            Student studentdelete = context.Students.FirstOrDefault(p => p.StudentID == txtStudentID.Text);
            if (studentdelete != null)
            {
                context.Students.Remove(studentdelete);
                context.SaveChanges();
                MessageBox.Show("Xóa sinh viên thành công!");
            }
            else
                MessageBox.Show("Không tìm thấy MSSV cần xóa!");
        }

        

        private void txtStudentID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtFullName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtAverageScore_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dgvData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                txtStudentID.Text = dgvData.Rows[index].Cells[0].Value.ToString();
                txtFullName.Text = dgvData.Rows[index].Cells[1].Value.ToString();
                cmbFaculty.Text = dgvData.Rows[index].Cells[2].Value.ToString();
                txtAverageScore.Text = dgvData.Rows[index].Cells[3].Value.ToString();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            bool check = true;
            check = CheckError();
            if (check)
            {
                AddStudent();
                Reset();
                ReLoadDataGridView();
            }
        }

        private void btnSua_Click_1(object sender, EventArgs e)
        {
            bool check = true;
            check = CheckError();
            if (check)
            {
                UpdateStudent();
                Reset();
                ReLoadDataGridView();
            }
        }
        private string imagePath; // Lưu đường dẫn tới hình ảnh đã chọn

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Hình ảnh|*.jpg;*.jpeg;*.png|Tất cả các tệp|*.*";
            

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                imagePath = openFileDialog.FileName;

                // Hiển thị hình ảnh trên PictureBox
                picAvatar.Image =Image.FromFile(imagePath);

                string studentID = txtStudentID.Text;
                string extension = Path.GetExtension(imagePath);
                string imageDirectory = Path.Combine(Application.StartupPath, "Images");
                string newImagePath = Path.Combine(imageDirectory, $"{studentID}{extension}");
                File.Copy(imagePath, newImagePath);
                MessageBox.Show("Them hinh anh thanh cong!");


                
            }
        }
    }
}
