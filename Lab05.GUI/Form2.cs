﻿using Lab05.BUS;
using Lab05.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab05.GUI
{
    public partial class frmRegister : Form
    {
        private readonly StudentService studentService = new StudentService();
        private readonly FacultyService facultyService = new FacultyService();
        private readonly MajorService majorService = new MajorService();
        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            cmbFaculty.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbMajor.DropDownStyle = ComboBoxStyle.DropDownList;
            try
            {
                var listFacultys = facultyService.GetAll();
                FillFacultyCombobox(listFacultys);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillFacultyCombobox(List<Faculty> listFacultys)
        {
            this.cmbFaculty.DataSource = listFacultys;
            this.cmbFaculty.DisplayMember = "FacultyName";
            this.cmbFaculty.ValueMember = "FacultyID";
        }

        private void cmbFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            Faculty selectedFaculty = cmbFaculty.SelectedItem as Faculty;
            if (selectedFaculty != null)
            {
                var listMajor = majorService.GetAllByFaculty(selectedFaculty.FacultyID);
                FillMajorCombobox(listMajor);
                var listStudents = studentService.GetAllHasNoMajor(selectedFaculty.FacultyID);
                BindGrid(listStudents);
            }
        }

        private void BindGrid(List<Student> listStudents)
        {
            dgvStudent.Rows.Clear();
            foreach (var item in listStudents)
            {
                int index = dgvStudent.Rows.Add();
                dgvStudent.Rows[index].Cells[1].Value = item.StudentID;
                dgvStudent.Rows[index].Cells[2].Value = item.FullName;
                if (item.Faculty != null)
                    dgvStudent.Rows[index].Cells[3].Value = item.Faculty.FacultyName;
                dgvStudent.Rows[index].Cells[4].Value = item.AverageScore + "";
                if (item.MajorID != null)
                    dgvStudent.Rows[index].Cells[5].Value = item.Major.Name + "";
            }
        }

        private void FillMajorCombobox(List<Major> listMajor)
        {
            this.cmbMajor.DataSource = listMajor;
            this.cmbMajor.DisplayMember = "Name";
            this.cmbMajor.ValueMember = "FacultyID";
            this.cmbMajor.ValueMember = "MajorID";
            
        }
        public void RegisterMajor(string studentID, string majorName)
        {
            StudentModel context = new StudentModel();

            // Tìm sinh viên theo ID
            Student student = context.Students.FirstOrDefault(s => s.StudentID == studentID);

            if (student != null)
            {
                // Tìm chuyên ngành theo tên
                Major major = context.Majors.FirstOrDefault(m => m.Name == majorName);

                if (major != null)
                {
                    // Gán chuyên ngành cho sinh viên
                    student.MajorID = major.MajorID;

                    // Lưu các thay đổi vào cơ sở dữ liệu
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Không tìm thấy chuyên ngành có tên: " + majorName);
                }
            }
            else
            {
                throw new Exception("Không tìm thấy sinh viên có ID: " + studentID);
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (dgvStudent.SelectedRows.Count >= 0)
            {


                DataGridViewRow selectedRow = dgvStudent.Rows[0];
                string studentID = selectedRow.Cells[1].Value.ToString(); // Convert to string
                string majorName = cmbMajor.Text;

                bool checkedCell = (bool)dgvStudent.Rows[0].Cells[0].Value;
                
                try
                {
                    StudentModel db = new StudentModel();
                    // Gọi phương thức để đưa dữ liệu vào cơ sở dữ liệu
                    Student existingStudent = db.Students.FirstOrDefault(s => s.StudentID == studentID);
                    //  existingStudent.Major = majorName;
                    RegisterMajor(studentID, majorName);
                    // Cập nhật lại DataGridView
                    var selectedFaculty = cmbFaculty.SelectedItem as Faculty;
                    var updatedStudents = studentService.GetAllHasNoMajor(selectedFaculty.FacultyID);
                    BindGrid(updatedStudents);

                    MessageBox.Show("Đăng kí chuyên ngành thành công!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Đăng kí chuyên ngành không thành công: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một sinh viên trước khi đăng ký chuyên ngành!!");
            }
        }
    }
}
